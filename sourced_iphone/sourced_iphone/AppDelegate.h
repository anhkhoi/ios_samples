//
//  AppDelegate.h
//  sourced_iphone
//
//  Created by Khoi Tran on 1/18/17.
//  Copyright © 2017 Khoi Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

