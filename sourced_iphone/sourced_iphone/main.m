//
//  main.m
//  sourced_iphone
//
//  Created by Khoi Tran on 1/18/17.
//  Copyright © 2017 Khoi Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
